﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.IO;
using System.Windows.Forms;


namespace NDE1000
{
    public class NDE
    {
        /*========================================
                        SETTING PORT
             ===========================================*/

        public SerialPort serialPortNya = new SerialPort();

        public void serialPort(string com) //"COM1" atau "COM2" atau ...
        {
            serialPortNya.PortName = com;
            serialPortNya.BaudRate = 9600;
            serialPortNya.Parity = Parity.Even;
            serialPortNya.StopBits = StopBits.One;
        }

        public void OpenPort()
        {
            serialPortNya.Open();   
        }

        public void ClosePort()
        {
            serialPortNya.Close();
        }

        /*===================================
                        SENDED COMMAND 
             ===================================*/
             
        // CMDC = command clear
        private byte[] CMDC_DISPENSE_NUMBER = { 0x02, 0x30, 0x30, 0x49, 0x30, 0x30, 0x30, 0x31 };
        private byte[] CMDC_ErrMSG = { 0x02, 0x30, 0x30, 0x49, 0x30, 0x30, 0x30, 0x32 };
        private byte[] CMDC_BOTH = { 0x02, 0x30, 0x30, 0x49, 0x30, 0x30, 0x30, 0x33 };
        // CMD = command
        private byte[] CMD_LOCK = { 0x02, 0x30, 0x30, 0x4b, 0x31, 0x30, 0x31, 0x30 };
        private byte[] CMD_UNLOCK = { 0x02, 0x30, 0x30, 0x4b, 0x30, 0x30, 0x30, 0x30 };
        private byte[] CMD_CHECK_STATUS = { 0x02, 0x30, 0x30, 0x53, 0x30, 0x30, 0x30, 0x30 };
        private byte[] CMD_CHECK_DISPENSE_NUMBER = { 0x02, 0x30, 0x30, 0x43, 0x30, 0x30, 0x30, 0x30 };
        private byte[] CMD_WRTIE_DATE = { 0x02, 0x30, 0x30, 0x54, 0x64, 0x13, 0x07, 0x1A };
        private byte[] CMD_WRTIE_TIME = { 0x02, 0x30, 0x30, 0x54, 0x74, 0x0E, 0x0B, 0x2B };
        private byte[] CMD_READ_DATE = { 0x02, 0x30, 0x30, 0x54, 0x64, 0x30, 0x30, 0x30 };
        private byte[] CMD_READ_TIME = { 0x02, 0x30, 0x30, 0x54, 0x74, 0x30, 0x30, 0x30 };

        private byte[] CMD_DISPENSE = { 0x02, 0x30, 0x30, 0x42, 0x30, 0x30, 0x30 };

        private byte[] FOOTER = { 0x03 };

        public byte[] messageSent = new byte[10];
        private string hexstring = "";
        /*=====================================
                        OTHER FUNCTION
             =======================================*/

        private byte[] CS(byte[] input)
        {
            string test = ByteArrayToString(input);
            int coba = IntComputeCheckSum(test);
            string hasilAkhir = ToHex(coba);
            byte[] hasilTerakhir = HexStringToBytes(hasilAkhir);
            return hasilTerakhir;
        }

        public string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public byte[] HexStringToBytes(string hexString)
        {
            try
            {
                if (hexString.Length >= 3) //must have minimum of length of 3
                    if (hexString[0] == '0' && (hexString[1] == 'x' || hexString[1] == 'X'))
                        hexString = hexString.Substring(2);
                int dataSize = (hexString.Length - 1) / 2;
                int expectedStringLength = 2 * dataSize;
                while (hexString.Length < expectedStringLength)
                    hexString = "0" + hexString; //zero padding in the front
                int NumberChars = hexString.Length / 2;
                byte[] bytes = new byte[NumberChars];
                using (var sr = new StringReader(hexString))
                {
                    for (int i = 0; i < NumberChars; i++)
                        bytes[i] = Convert.ToByte(new string(new char[2] { (char)sr.Read(), (char)sr.Read() }), 16);
                }
                return bytes;
            }
            catch
            {
                return null;
            }

        }

        public int IntComputeCheckSum(string values)
        {
            int count = 0;
            string Conv;

            //Group each two bits together...
            for (int x = 0; x < values.Length; x = x + 2)
            {
                Conv = values[x].ToString() + values[x + 1].ToString();
                int toParse = int.Parse(Conv, System.Globalization.NumberStyles.HexNumber);
                //Add these to overall count
                count += int.Parse(Conv, System.Globalization.NumberStyles.HexNumber);
            }

            //Count mod 0x100 or 256(int)
            count = count % 256;
            return count;

        }

        public string ToHex(int value)
        {
            return String.Format("{0:X}", value);
        }

        //public string Receive()
        //{
        //    int length = serialPortNya.BytesToRead;
        //    byte[] buf = new byte[length];
        //    if (serialPortNya.BytesToRead > 0)
        //    {
        //        serialPortNya.Read(buf, 0, length);
        //    }
        //    string coba = BitConverter.ToString(buf, 0 ,buf.Length);
        //    return coba;    
        //}

        //private void DataReceiveHandler(object sender, SerialDataReceivedEventArgs e)
        //{
        //    SerialPort sp = (SerialPort)sender;
        //    string indata = sp.ReadExisting();
        //    byte[] ba = Encoding.Default.GetBytes(indata);
        //    var hexstring = BitConverter.ToString(ba);
        //    hexstring = hexstring.Replace("-", " ");
        //}

        /*================================
                    CALLED FUNCTION 
            ==================================*/


        public void ClearDispenseNumber(ref string stat, ref string value)
        {
            byte[] Rsp = new byte[2];
            byte[] CheckSum = CS(CMDC_DISPENSE_NUMBER);
            messageSent = CMDC_DISPENSE_NUMBER.Concat(CheckSum.Concat(FOOTER)).ToArray();
            serialPortNya.Write(messageSent, 0, messageSent.Length);

            for (int loop = 0; loop < 3; loop++)
            {
                if (serialPortNya.BytesToRead > 0)
                {
                    serialPortNya.Read(Rsp, 0, 1);
                    switch (Rsp[0])
                    {
                        case 0x06:
                            stat = "ACK";
                            break;
                        case 0x0A:
                            stat = "NACK";
                            break;
                        case 0x3F:
                            stat = "Coding Jelek";
                            break;
                    }
                    value = BitConverter.ToString(Rsp, 0, Rsp.Length);
                }           
            }
        }

        public void ClearErrorMessage(ref string stat, ref string value)
        {
            byte[] Rsp = new byte[2];
            byte[] CheckSum = CS(CMDC_ErrMSG);
            messageSent = CMDC_ErrMSG.Concat(CheckSum.Concat(FOOTER)).ToArray();
            serialPortNya.Write(messageSent, 0, messageSent.Length);

            for (int loop = 0; loop < 3; loop++)
            {
                if (serialPortNya.BytesToRead > 0)
                {
                    serialPortNya.Read(Rsp, 0, 1);
                    switch (Rsp[0])
                    {
                        case 0x06:
                            stat = "ACK";
                            break;
                        case 0x0A:
                            stat = "NACK";
                            break;
                        case 0x3F:
                            stat = "Coding Jelek";
                            break;
                    }
                    value = BitConverter.ToString(Rsp, 0, Rsp.Length);
                }
            }
        }

        public void ClearBoth(ref string stat, ref string value)
        {
            byte[] Rsp = new byte[2];
            byte[] CheckSum = CS(CMDC_BOTH);
            messageSent = CMDC_BOTH.Concat(CheckSum.Concat(FOOTER)).ToArray();
            serialPortNya.Write(messageSent, 0, messageSent.Length);

            for (int loop = 0; loop < 3; loop++)
            {
                if (serialPortNya.BytesToRead > 0)
                {
                    serialPortNya.Read(Rsp, 0, 1);
                    switch (Rsp[0])
                    {
                        case 0x06:
                            stat = "ACK";
                            break;
                        case 0x0A:
                            stat = "NACK";
                            break;
                        case 0x3F:
                            stat = "Coding Jelek";
                            break;
                    }
                    value = BitConverter.ToString(Rsp, 0, 1);
                }
            }
        }

        //public string Lock()
        //{
        //    byte[] CheckSum = CS(CMD_LOCK);
        //    messageSent = CMD_LOCK.Concat(CheckSum.Concat(FOOTER)).ToArray();
        //    serialPortNya.Write(messageSent, 0, messageSent.Length);
        //    string value = Receive();
        //    return value;
        //}

        //public string UnLock()
        //{
        //    byte[] CheckSum = CS(CMD_UNLOCK);
        //    messageSent = CMD_UNLOCK.Concat(CheckSum.Concat(FOOTER)).ToArray();
        //    serialPortNya.Write(messageSent, 0, messageSent.Length);
        //    string value = Receive();
        //    return value;
        //}

        //public string CheckStatus()
        //{
        //    byte[] CheckSum = CS(CMD_CHECK_STATUS);
        //    messageSent = CMD_CHECK_STATUS.Concat(CheckSum.Concat(FOOTER)).ToArray();
        //    serialPortNya.Write(messageSent, 0, messageSent.Length);
        //    string value = Receive();
        //    return value;
        //}

        //public string CheckDispenseNumber()
        //{
        //    byte[] CheckSum = CS(CMD_CHECK_DISPENSE_NUMBER);
        //    messageSent = CMD_CHECK_DISPENSE_NUMBER.Concat(CheckSum.Concat(FOOTER)).ToArray();
        //    serialPortNya.Write(messageSent, 0, messageSent.Length);
        //    string value = Receive();
        //    return value;
        //}

        public void WriteDate()
        {
            byte[] CheckSum = CS(CMD_WRTIE_DATE);
            messageSent = CMD_WRTIE_DATE.Concat(CheckSum.Concat(FOOTER)).ToArray();
            serialPortNya.Write(messageSent, 0, messageSent.Length);
        }
        public void WriteTime()
        {
            byte[] CheckSum = CS(CMD_WRTIE_DATE);
            messageSent = CMD_WRTIE_TIME.Concat(CheckSum.Concat(FOOTER)).ToArray();
            serialPortNya.Write(messageSent, 0, messageSent.Length);
        }
        //public string ReadDate()
        //{
        //    byte[] CheckSum = CS(CMD_WRTIE_DATE);
        //    messageSent = CMD_READ_DATE.Concat(CheckSum.Concat(FOOTER)).ToArray();
        //    serialPortNya.Write(messageSent, 0, messageSent.Length);
        //    string value = Receive();
        //    return value;
        //}
        //public string ReadTime()
        //{
        //    byte[] CheckSum = CS(CMD_WRTIE_DATE);
        //    messageSent = CMD_READ_TIME.Concat(CheckSum.Concat(FOOTER)).ToArray();
        //    serialPortNya.Write(messageSent, 0, messageSent.Length);
        //    string value = Receive();
        //    return value;
        //}

        //public string Dispense(string jmlh) //jmlh =  textbox jumlah dispense
        //{
        //    //string jmlh = textBox1.Text;
        //    byte[] jumlah = Encoding.ASCII.GetBytes(jmlh);
        //    byte[] tambahan = CMD_DISPENSE.Concat(jumlah).ToArray();
        //    byte[] CheckSum = CS(tambahan);
        //    messageSent = tambahan.Concat(CheckSum.Concat(FOOTER)).ToArray();
        //    serialPortNya.Write(messageSent, 0, messageSent.Length);
        //    string value = Receive();
        //    return value;
        //}
    }
}
